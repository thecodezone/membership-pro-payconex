<?php
/**
 * Plugin Name: WPMUDEV Membership 2 BlueFin PayConex Gateway
 * Plugin URI: https://www.codezone.io/
 * Description: BlueFin Payconex Payment Gateway for the MPMU DEV Connect 2 Plugin.
 * Version: 1.0
 * Author: CodeZone
 * Author URI: http://codezone.io/
 **/

require __DIR__ . '/vendor/autoload.php';

new Membership_Pro_PayConex_Plugin();
