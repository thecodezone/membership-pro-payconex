<?php

class MS_Gateway_PayConex_View_Form extends MS_View {

	public function to_html() {

		// let 3rd party themes/plugins use their own form
		if ( ! apply_filters( 'ms_gateway_payconex_view_form_to_html', true, $this ) ) {
			return;
		}

		$fields = $this->prepare_fields();
		ob_start();
		// Render tabbed interface.
		?>
			<div class="ms-wrap">
				<?php if ( $this->data['error'] ): ?>
					<div class="ms-validation-error"><p><?php echo $this->data['error']; ?></p></div>
				<?php endif; ?>

				<?php $this->render_existing( $fields ); ?>

				<form id="ms-authorize-extra-form" method="post" class="ms-form">
					<?php foreach ( $fields['hidden'] as $field ): ?>
						<?php MS_Helper_Html::html_element( $field ); ?>
					<?php endforeach;?>

					<div id="ms-authorize-card-wrapper">
						<table class="form-table ms-form-table">
							<tr>
								<td class="ms-title-row" colspan="2">
									<?php _e( 'Add Credit Card', 'membership2' ); ?>
								</td>
							</tr>
							<tr>
								<td class="ms-card-info" colspan="2">
									<table border="0" width="100%" cellspacing="0" cellpadding="0">
										<tr>
										<td class="ms-col-card_num">
											<?php MS_Helper_Html::html_element( $fields['card']['card_num'] ); ?>
										</td>
										<td class="ms-col-card_code">
											<?php MS_Helper_Html::html_element( $fields['card']['card_code'] ); ?>
										</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td class="ms-col-expire" colspan="2">
									<?php MS_Helper_Html::html_element( $fields['card']['exp_month'] ); ?>
									<?php MS_Helper_Html::html_element( $fields['card']['exp_year'] ); ?>
								</td>
							</tr>
							<tr>
								<td class="ms-title-row" colspan="2">
									<?php _e( 'Billing Information', 'membership2' ); ?>
								</td>
							</tr>
							<tr>
								<td class="ms-col-first_name">
									<?php MS_Helper_Html::html_element( $fields['billing']['first_name'] ); ?>
								</td>
								<td class="ms-col-last_name">
									<?php MS_Helper_Html::html_element( $fields['billing']['last_name'] ); ?>
								</td>
							</tr>
							<tr>
								<td class="ms-col-country" colspan="2">
									<?php MS_Helper_Html::html_element( $fields['billing']['country'] ); ?>
								</td>
							</tr>
							<?php if ( ! empty( $fields['extra'] ) ) : ?>
							<?php foreach ( $fields['extra'] as $field ) : ?>
							<tr>
								<td class="ms-col-<?php echo esc_attr( $field['id'] ); ?>" colspan="2">
									<?php MS_Helper_Html::html_element( $field ); ?>
								</td>
							</tr>
							<?php endforeach; ?>
							<?php endif; ?>
							<tr>
								<td class="ms-col-submit" colspan="2">
									<?php MS_Helper_Html::html_element( $fields['submit'] ); ?>
								</td>
							</tr>
						</table>
					</div>
				</form>
				<div class="clear"></div>
			</div>
		<?php
		$html = ob_get_clean();

		echo $html;
	}

	public function prepare_fields() {
		$currency = MS_Plugin::instance()->settings->currency;
		$fields['hidden'] = array(
			'gateway' => array(
				'id' 	=> 'gateway',
				'type' 	=> MS_Helper_Html::INPUT_TYPE_HIDDEN,
				'value' => $this->data['gateway'],
			),
			'ms_relationship_id' => array(
				'id' 	=> 'ms_relationship_id',
				'type' 	=> MS_Helper_Html::INPUT_TYPE_HIDDEN,
				'value' => $this->data['ms_relationship_id'],
			),
			'_wpnonce' => array(
				'id' 	=> '_wpnonce',
				'type' 	=> MS_Helper_Html::INPUT_TYPE_HIDDEN,
				'value' => wp_create_nonce( "{$this->data['gateway']}_{$this->data['ms_relationship_id']}" ),
			),
			'action' => array(
				'id' 	=> 'action',
				'type' 	=> MS_Helper_Html::INPUT_TYPE_HIDDEN,
				'value' => $this->data['action'],
			),
			'step' => array(
				'id' 	=> 'step',
				'type' 	=> MS_Helper_Html::INPUT_TYPE_HIDDEN,
				'value' => MS_Controller_Frontend::STEP_PROCESS_PURCHASE,
			),
		);

		$months = array( '' => __( 'Month', 'membership2' ) );
		for ( $i = 1, $date = new DateTime( '01-01-1970' ); $i <= 12; $date->setDate( 2013, ++$i, 1 ) ) {
			$months[ $i ] = $date->format( 'm - M' );
		}

		$years = array( '' => __( 'Year', 'membership2' ) );
		for ( $i = gmdate( 'Y' ), $maxYear = $i + 15; $i <= $maxYear; $i++ ) {
			$years[ $i ] = $i;
		}

		$fields['card'] = array(
			'card_num' => array(
				'id' 			=> 'card_num',
				'title' 		=> __( 'Card Number', 'membership2' ),
				'type' 			=> MS_Helper_Html::INPUT_TYPE_TEXT,
				'placeholder' 	=> '•••• •••• •••• ••••',
				'maxlength' 	=> 24, // 20 digits + 4 spaces
			),
			'card_code' => array(
				'id' 			=> 'card_code',
				'title' 		=> __( 'Card Code', 'membership2' ),
				'type' 			=> MS_Helper_Html::INPUT_TYPE_TEXT,
				'placeholder' 	=> 'CVC',
				'maxlength' 	=> 4,
			),
			'exp_month' => array(
				'id' 			=> 'exp_month',
				'title' 		=> __( 'Expires', 'membership2' ),
				'type' 			=> MS_Helper_Html::INPUT_TYPE_SELECT,
				'field_options' => $months,
				'class' 		=> 'ms-select',
			),
			'exp_year' => array(
				'id' 			=> 'exp_year',
				'type' 			=> MS_Helper_Html::INPUT_TYPE_SELECT,
				'field_options' => $years,
				'class' 		=> 'ms-select',
			),
		);

        [
            'first_name' => substr(trim(filter_input(INPUT_POST, 'first_name')), 0, 50),
            'last_name' => substr(trim(filter_input(INPUT_POST, 'last_name')), 0, 50),
            'company' => substr(trim(filter_input(INPUT_POST, 'company')), 0, 50),
            'street_address1' => substr(trim(filter_input(INPUT_POST, 'address')), 0, 60),
            'city' => substr(trim(filter_input(INPUT_POST, 'city')), 0, 40),
            'state' => substr(trim(filter_input(INPUT_POST, 'state')), 0, 40),
            'zip' => substr(trim(filter_input(INPUT_POST, 'zip')), 0, 20),
            'country' => substr(trim(filter_input(INPUT_POST, 'country')), 0, 60),
            'phone' => substr(trim(filter_input(INPUT_POST, 'phone')), 0, 25)
        ];

		$fields['billing'] = array(
			'first_name' 		=> array(
				'id' 			=> 'first_name',
				'title' 		=> __( 'First Name', 'membership2' ),
				'type' 			=> MS_Helper_Html::INPUT_TYPE_TEXT,
				'placeholder' 	=> __( 'First Name', 'membership2' ),
			),
			'last_name' => array(
				'id' 			=> 'last_name',
				'title' 		=> __( 'Last Name', 'membership2' ),
				'type' 			=> MS_Helper_Html::INPUT_TYPE_TEXT,
				'placeholder' 	=> __( 'Last Name', 'membership2' ),
			),
			'country' => array(
				'id' 			=> 'country',
				'title' 		=> __( 'Country', 'membership2' ),
				'type' 			=> MS_Helper_Html::INPUT_TYPE_SELECT,
				'field_options' => $this->data['countries'],
				'class' 		=> 'ms-select',
			),
		);
		$fields['submit'] = array(
			'id' 		=> 'submit',
			'type' 		=> MS_Helper_Html::INPUT_TYPE_SUBMIT,
			'value' 	=> __( 'Pay now', 'membership2' ),
		);

		// Can be populated via the filter to add extra fields to the form.
		$fields['extra'] = array();

		if ( 'update_card' == $this->data['action'] ) {
			$fields['submit']['value'] = __( 'Change card', 'membership2' );
		}

		return apply_filters(
			'ms_gateway_authorize_view_form_prepare_fields',
			$fields
		);
	}

	/**
	 * Renders Authorize.net CIM profiles.
	 *
	 * @since  1.0.0
	 *
	 * @access protected
	 */
	protected function render_existing( $fields ) {
		// if profile is empty, then return
        if (!$this->data['token_id']) {
            return;
        }


        $token = array(
			'id' => 'token_id',
			'type' => MS_Helper_Html::INPUT_TYPE_HIDDEN,
			'field_options' => $options,
			'value' => $this->data['token_id'] ,
		);

		?>
		<form id="ms-gateway-membership-extra-form" method="post" class="ms-form">
            <?php MS_Helper_Html::html_element( $token ); ?>
			<?php foreach ( $fields['hidden'] as $field ): ?>
				<?php MS_Helper_Html::html_element( $field ); ?>
			<?php endforeach;?>

			<div id="ms-authorize-cim-profiles-wrapper" class="authorize-form-block">
				<table>
					<tr>
						<td class="ms-title-row">Use Stored Card</td>
					</tr>
					<tr class="ms-row-submit">
						<td class="ms-col-submit">
							<?php MS_Helper_Html::html_element( array(
                                'id' 		=> 'submit',
                                'type' 		=> MS_Helper_Html::INPUT_TYPE_SUBMIT,
                                'value' 	=> 'Use card ending in ' . $this->data['card_num'],
                            ) ); ?>
						</td>
					</tr>
				</table>
			</div>
		</form>
		<?php
	}
}
