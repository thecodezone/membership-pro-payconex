<?php

/**
 * Class Membership_Pro_QuickSwipe_Factory
 */
class Membership_Pro_QuickSwipe_Factory
{
    public static function  make($accountId, $accessKey, $sandbox) {
        $qs = new \QuickSwipePost($sandbox);

        $qs->setParam('account_id', $accountId);
        $qs->setParam('api_accesskey',$accessKey);
        $qs->setParam('ip_address', $_SERVER['REMOTE_ADDR']);

        return $qs;
    }
}
