<?php

/**
 * Class Plugin
 */
class Membership_Pro_PayConex_Plugin
{
    const ID = 'payconex';
    const gatewayClass = MS_Gateway_PayConex::class;

    public function __construct()
    {
        $this->register();
    }

    protected function register()
    {
        add_filter('plugins_loaded', function () {
            $this->registerGateway();
        });
    }

    public function factory()
    {
        return MS_Factory::load(self::gatewayClass);
    }

    protected function registerGateway()
    {
        add_filter('ms_model_gateway_files', function($files) {
            $reflection = new ReflectionClass(self::gatewayClass);

            $path = $reflection->getFileName();
            $filename = substr($path, strrpos($path, '/') + 1);
            $files[$filename] = 'plugins/membership-pro-payconex/src/' . $filename;

            return $files;
        });

        add_filter('ms_model_gateway_get_gateways', function($gateways) {
            $gateway = $this->factory();
            $gateways[self::ID] = $gateway;

            return $gateways;
        });


        add_filter('ms_gateway_view_button_data', function($data) {
            if ($data['gateway']->id === self::ID) {
                $data['step'] = 'gateway_form';
            }
            return $data;
        });

        add_filter('ms_gateway_view_form_data', function($data) {
            if ($data['gateway'] === self::ID) {
                $subscription = MS_Factory::load(
                    'MS_Model_Relationship',
                    $data['ms_relationship_id']
                );
                $member = $subscription->get_member();
                $data['token_id'] = $member->get_gateway_profile(
                    self::ID,
                    'token_id'
                );
                $data['card_num'] = $member->get_gateway_profile(
                    self::ID,
                    'card_num'
                );
                $data['countries'] 	= $this->factory()->get_country_codes();
                $data['action'] 	= $_REQUEST['action'];
                $data['error'] = $_POST['error'];
            }
            return $data;
        });

        add_filter('ms_gateway_view_form', function($view) {
            //Work around bug in plugin where it doesn't pass data
            $filtered = array_filter(debug_backtrace(), function($array) {
                return (strpos($array['file'], 'class-ms-controller-gateway') !== false);
            });
            $gateway_id = $filtered[array_key_first($filtered)]['args'][2];

            if ($gateway_id === self::ID) {
                $view = MS_Factory::create( 'MS_Gateway_PayConex_View_Form' );
            }

            return $view;
        });

        add_action( 'ms_controller_gateway_form_error', function($e) {
            if (strpos($e->getFile(), 'payconex') !== false) {
                $message = $e->getMessage();
                $message = trim(substr($message, strpos($message, '|') + 1));
                $message = trim(substr($message, 0, strpos($message, "(")));
                $message = str_replace('_', ' ', $message);
                $_POST['error'] = sprintf(
                    __( 'Error: %s', 'membership2' ),
                    $message
                );

                do_action( 'ms_controller_frontend_signup_gateway_form' );
            }
        });
    }
}
