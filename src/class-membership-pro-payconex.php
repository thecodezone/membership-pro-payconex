<?php

/**
 * Class MS_Gateway_PayConex
 */
class MS_Gateway_PayConex extends MS_Gateway
{
    const ID = 'payconex';

    /**
     * Gateway singleton instance.
     *
     * @since  1.0.0
     * @var string $instance
     */
    public static $instance;

    /**
     * BlueFin's Customer Information Manager wrapper.
     *
     * @since  1.0.0
     * @var string $cim
     */
    protected static $cim = '';

    /**
     * BlueFin API login IP.
     *
     * @see    @link https://www.authorize.net/support/CP/helpfiles/Account/Settings/Security_Settings/General_Settings/API_Login_ID_and_Transaction_Key.htm
     * @since  1.0.0
     * @var string $api_login_id
     */
    protected $api_account_id = '';

    /**
     * BlueFin API transaction key.
     *
     * @since  1.0.0
     * @var string $api_transaction_key
     */
    protected $api_access_key = '';

    /**
     * BlueFin API login IP.
     *
     * @see    @link https://www.authorize.net/support/CP/helpfiles/Account/Settings/Security_Settings/General_Settings/API_Login_ID_and_Transaction_Key.htm
     * @since  1.0.0
     * @var string $api_login_id
     */
    protected $sandbox_account_id = '';

    /**
     * BlueFin API transaction key.
     *
     * @since  1.0.0
     * @var string $api_transaction_key
     */
    protected $sandbox_access_key = '';

    /**
     * Initialize the object.
     *
     * @since  1.0.0
     * @internal
     */
    public function after_load()
    {
        parent::after_load();
        $this->id = self::ID;
        $this->name = __('PayConex Gateway', 'membership2');
        $this->group = 'Bluefin';
        $this->manual_payment = true; // Recurring billed/paid manually
        $this->pro_rate = true;
    }

    /**
     * Get a fresh instance of the bluefin quickswipe sdk
     *
     * @return QuickSwipePost
     */
    protected function make_quick_swipe()
    {
        return Membership_Pro_QuickSwipe_Factory::make($this->get_account_id(), $this->get_access_key(), !$this->is_live_mode());
    }


    /**
     * Get the stored token ID
     *
     * @param $member MS_Api_Member
     *
     * @return mixed
     */
    public function get_token_id($member)
    {
        $token_id = $member->get_gateway_profile(
            self::ID,
            'token_id'
        );

        if ($this->get_post_token_id()) {
            //Make sure the ID in the post is the same as is stored
            if ($this->get_post_token_id() === $token_id) {
                return $token_id;
            }

            return null;
        }


        return apply_filters(
            'ms_gateway_payconex_get_token_id',
            $token_id,
            $member,
            $this
        );
    }

    /**
     * Get the stored CVV code.
     * @param $member
     *
     * @return mixed
     */
    protected function get_card_code($member)
    {
        return $member->get_gateway_profile(
            self::ID,
            'card_code'
        );
    }

    /**
     * Get the stored billing address.
     * @param $member
     *
     * @return mixed
     */
    protected function get_billing_address($member)
    {
        return $member->get_gateway_profile(
            self::ID,
            'billing_address'
        );
    }


    /**
     * Get the formatted card number from the post.
     * @return string|string[]|null
     */
    public function get_post_card_num()
    {
        return preg_replace( '/\D/', '', filter_input( INPUT_POST, 'card_num' ) );
    }

    /**
     * Get the formatted card number from the post.
     * @return string|string[]|null
     */
    public function get_post_token_id()
    {
        return preg_replace( '/\D/', '', filter_input( INPUT_POST, 'token_id' ) );
    }

    /**
     * Get the last four digits of the card number from the post.
     * @return bool|string
     */
    public function get_post_card_num_last_four()
    {
        return substr($this->get_post_card_num(), -4);
    }

    /**
     * Get the card expiration year from the post.
     * @return mixed
     */
    public function get_post_card_exp_year()
    {
        return filter_input( INPUT_POST, 'exp_year', FILTER_VALIDATE_INT );
    }

    /**
     * Get the card expiration month from the post.
     * @return bool|string
     */
    public function get_post_card_exp_month()
    {
        return substr( filter_input( INPUT_POST, 'exp_month', FILTER_VALIDATE_INT ), -2 );
    }

    /**
     * Get the card expiration from the post as a date.
     * @return false|string|null
     */
    public function get_post_card_expiration()
    {
        if (!$this->get_post_card_exp_year() || !$this->get_post_card_exp_month()) {
            return null;
        }
        return date( 'my', strtotime( "{$this->get_post_card_exp_year()}-{$this->get_post_card_exp_month()}-01" ) );
    }

    /**
     * Get the card CVV from the post.
     * @return mixed
     */
    public function get_post_card_code()
    {
        return filter_input(INPUT_POST, 'card_code');
    }

    /**
     * Get the post billing address as an array formatted for payconex.
     * @return array
     */
    public function get_post_billing_address()
    {
        return [
                'first_name' => substr(trim(filter_input(INPUT_POST, 'first_name')), 0, 50),
                'last_name' => substr(trim(filter_input(INPUT_POST, 'last_name')), 0, 50),
                'company' => substr(trim(filter_input(INPUT_POST, 'company')), 0, 50),
                'street_address1' => substr(trim(filter_input(INPUT_POST, 'address')), 0, 60),
                'city' => substr(trim(filter_input(INPUT_POST, 'city')), 0, 40),
                'state' => substr(trim(filter_input(INPUT_POST, 'state')), 0, 40),
                'zip' => substr(trim(filter_input(INPUT_POST, 'zip')), 0, 20),
                'country' => substr(trim(filter_input(INPUT_POST, 'country')), 0, 60),
                'phone' => substr(trim(filter_input(INPUT_POST, 'phone')), 0, 25)
        ];
    }


    /**
     * Store card info from post in payconex
     *
     * @param $member
     *
     * @return mixed
     * @throws Membership_Pro_QuickSwipe_Exception
     */
    protected function store_card_in_payconex ($member, $invoice) {

        $qs = $this->make_quick_swipe();
        $transactionType = 'STORE';
        $error = '';

        $qs->setParams([
            'tender_type' => 'CARD',
            'transaction_type' => 'STORE',
            'card_number' => $this->get_post_card_num(),
            'card_expiration' => $this->get_post_card_expiration()
        ]);


        $result = $qs->process();
        $response = $qs->getResponse();

        if ($result) {
            $member->set_gateway_profile(
                self::ID,
                'token_id',
                $response['transaction_id']
            );

            $this->save_card_info($member);


            do_action(
                'ms_gateway_store_card_in_payconex_after',
                $member,
                $this
            );

            return $response;

        } else {
            $error = $response['error_message'];
            if ( ! empty($response['authorization_message'])) {
                $message = htmlentities("$transactionType | $error: $response[authorization_message]");
            } else {
                $message = htmlentities("$transactionType | $error ($response[error_code])");
            }

            $this->transaction_log($transactionType, false, $invoice, $message);

            throw new Exception($message);
        }
    }

    /**
     * Processes purchase action.
     *
     * This function is called when a payment was made: We check if the
     * transaction was successful. If it was we call `$invoice->changed()` which
     * will update the membership status accordingly.
     *
     * @param $subscription
     *
     * @return mixed
     * @throws Membership_Pro_QuickSwipe_Exception
     */
    public function process_purchase($subscription)
    {
        do_action(
            'ms_gateway_payconex_process_purchase_before',
            $subscription,
            $this
        );

        $invoice = $subscription->get_current_invoice();
        $member = $subscription->get_member();
        $token_id = $this->get_post_token_id();

        if (!$token_id) {
            $this->store_card_in_payconex($member, $invoice);
        }



        if ( ! $invoice->is_paid()) {
            // Not paid yet, request the transaction.'
            $this->online_purchase($invoice, $member, 'process');
        } elseif (0 == $invoice->total) {
            // Paid and free.
            $invoice->changed();
        }


        $invoice->gateway_id = self::ID;
        $invoice->save();

        return apply_filters(
            'ms_gateway_payconex_process_purchase',
            $invoice,
            $this
        );
    }

    /**
     * Request automatic payment to the gateway.
     *
     * @since  1.0.0
     *
     * @param MS_Model_Relationship $subscription The related membership relationship.
     *
     * @return bool True on success.
     */
    public function request_payment($subscription)
    {
        do_action(
            'ms_gateway_payconex_request_payment_before',
            $subscription,
            $this
        );

        $member = $subscription->get_member();
        $invoice = $subscription->get_current_invoice();

        if ( ! $invoice->is_paid()) {
            // Not paid yet, request the transaction.
            $was_paid = $this->online_purchase($invoice, $member, 'request');
        } else {
            // Invoice was already paid earlier.
            $was_paid = true;
        }

        do_action(
            'ms_gateway_payconex_request_payment_after',
            $subscription,
            $was_paid,
            $this
        );

        return $was_paid;
    }

    /**
     * Save card info.
     *
     * Save only 4 last digits and expire date.
     *
     * @since  1.0.0
     * @param MS_Model_Member $member The member to save card info.
     */
    public function save_card_info( $member ) {


        $member->set_gateway_profile(
            self::ID,
            'card_num',
            $this->get_post_card_num_last_four()
        );

        $member->set_gateway_profile(
            self::ID,
            'card_exp',
            $this->get_post_card_expiration()
        );

        $member->set_gateway_profile(
            self::ID,
            'card_code',
            $this->get_post_card_code()
        );

        $member->set_gateway_profile(
            self::ID,
            'billing_address',
            $this->get_post_billing_address()
        );

        do_action(
            'ms_gateway_payconex_card_info_after',
            $member,
            $this
        );
    }

    /**
     * Processes online payments.
     *
     * Send to process the payment immediatly.
     *
     * @since  1.0.0
     *
     * @param MS_Model_Invoice $invoice The invoice to pay.
     * @param MS_Model_Member The member paying the invoice.
     *
     * @return bool True on success, otherwise throws an exception.
     */
    protected function online_purchase(&$invoice, $member, $log_action)
    {
        $success = false;
        $notes = '';
        $amount = 0;
        $external_id = null;
        $token_id = $this->get_token_id($member);
        $cvv = $this->get_card_code($member);
        $billing_address = $this->get_billing_address($member);
        $error = '';

        do_action(
            'ms_gateway_payconex_online_purchase_before',
            $invoice,
            $member,
            $this
        );

        $_POST['API Out: InvoiceNumber'] = $invoice->get_invoice_number();

        if (0 == $invoice->total) {
            $notes = __('Total is zero. Payment approved. Not sent to gateway.', 'membership2');
            $invoice->pay_it(MS_Gateway_Free::ID, '');
            $invoice->add_notes($notes);
            $invoice->save();
            $invoice->changed();
        } else {
            $amount = MS_Helper_Billing::format_price($invoice->total);
            $invoice->timestamp = time();
            $invoice->save();


            $qs = $this->make_quick_swipe();

            $qs->setParam('transaction_type', 'SALE');
            $qs->setParam('transaction_amount', $amount);
            $qs->setParam('tender_type', 'CARD');
            $qs->setParam('token_id', $token_id);
            $qs->setParam('reissue', true);
            $qs->setParams($billing_address);
            $qs->setParam('card_verification', $cvv);
            $qs->setParam('transaction_description', $invoice->get_notes_desc());

            $result = $qs->process();

            $response = $qs->getResponse();

            if ($result) {
                $external_id = $response['transaction_id'];
                $invoice->pay_it(self::ID, $external_id);
                $success = true;
                $notes = __('Payment successful', 'membership2');

            } else {
                $error = $response['error_message'];
                if ( ! empty($response['authorization_message'])) {
                    $notes = htmlentities("$error: $response[authorization_message]");
                } else {
                    $notes = htmlentities("$error ($response[error_code])");
                }
            }

            $_POST['API Response: JSON'] = json_encode($response);
            $_POST['API Response: Short'] = $notes;
        }

        $this->transaction_log($log_action, $success, $invoice, $notes, $external_id);

        if (!$success) {
            throw new Exception($error);
        }

        return $success;
    }

    protected function transaction_log($log_action, $success, $invoice, $notes, $external_id = null)
    {
        $subscription = $invoice->get_subscription();
        $card_num = '';
        $card_code = '';
        if (isset($_POST['card_num'])) {
            // Card Num   6789765435678765
            // Becomes    ************8765
            $card_num = str_replace(' ', '', $_POST['card_num']);
            $_POST['card_num'] = str_pad(
                substr($card_num, -4),
                strlen($card_num),
                '*',
                STR_PAD_LEFT
            );
        }
        if (isset($_POST['card_code'])) {
            $card_code = $_POST['card_code'];
            $_POST['card_code'] = str_repeat('*', strlen($card_code));
        }


        do_action(
            'ms_gateway_transaction_log',
            self::ID, // gateway ID
            $log_action, // request|process|handle
            $success, // success flag
            $subscription->id, // subscription ID
            $invoice->id, // invoice ID
            MS_Helper_Billing::format_price($invoice->total), // charged amount
            $notes, // Descriptive text
            $external_id // External ID
        );

        // Restore the POST data in case it's used elsewhere.
        $_POST['card_num'] = $card_num;
        $_POST['card_code'] = $card_code;
    }

    /**
     * Get PayConex account id
     *
     * @since  1.0.0
     * @api
     *
     * @return string The PayConex account id
     */
    public function get_account_id()
    {
        $account_id = null;

        if ($this->is_live_mode()) {
            $account_id = $this->api_account_id;
        } else {
            $account_id = $this->sandbox_account_id;
        }

        return apply_filters(
            'ms_gateway_payconex_get_account_id',
            $account_id
        );
    }

    /**
     * Get PayConex secret key.
     *
     * @since    1.0.0
     * @internal The secret key should not be used outside this object!
     *
     * @return string The PayConex API secret key.
     */
    public function get_access_key()
    {
        $access_key = null;

        if ($this->is_live_mode()) {
            $access_key = $this->api_access_key;
        } else {
            $access_key = $this->sandbox_access_key;
        }

        return apply_filters(
            'ms_gateway_payconex_get_access_key',
            $access_key
        );
    }

    /**
     *
     *
     * /**
     * Verify required fields.
     *
     * @since  1.0.0
     * @api
     *
     * @return boolean True if configured.
     */
    public function is_configured()
    {
        $id = $this->get_account_id();
        $key = $this->get_access_key();

        $is_configured = ! (empty($id) || empty($key));

        return apply_filters(
            'ms_gateway_payconex_is_configured',
            $is_configured
        );
    }
}
