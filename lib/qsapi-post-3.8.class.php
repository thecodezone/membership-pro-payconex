<?php
class QuickSwipePost {

  private $url;
  private $params = array();
  private $response = array();

  public function __construct($sandbox = false) {
      if ($sandbox) {
        $this->url = 'https://cert.payconex.net/api/qsapi/3.8/';  // CERT
      } else {
        $this->url = 'https://secure.payconex.net/api/qsapi/3.8/'; //PROD
      }
  }

  public function setParam($pname, $value) {
    $this->params[$pname] = $value;
  }

  public function setParams($params) {
    if (is_array($params)) {
      $this->params = array_merge($this->params, $params);
    } else {
      die('Must send array');
    }
  }

  public function showParams($verbose = FALSE) {
    $output = get_class($this) . ":\n" . var_export($this->params, TRUE) . "\n";

    if ($verbose) {
      $pdata = '';
      foreach ($this->params AS $key=>$val) {
        $pdata .= rawurlencode($key)."=".rawurlencode($val)."&";
      }
      $pdata = substr($pdata, 0, -1);

      $output .= "\n$this->url?$pdata\n\n";
    }

    return $output;
  }

  public function getResponse() {
    return $this->response;
  }

  public function process() {
    // JSONed output
    $this->setParam('response_format','JSON');

    // JSONed input
    $pdata = array ('request_format' => 'JSON',
                    'params' => json_encode($this->params)
                    );

    $ch = curl_init($this->url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_POST, TRUE);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $pdata);

    $this->response = json_decode(curl_exec($ch),TRUE);

    return ($this->response['error_code'] === 0);
  }
}
?>
